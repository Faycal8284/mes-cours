ctrl + c : **copie**r

ctrl + v : **coller**

ctrl + d : **dupliquer**

ctrl + espace : **ouvre le dossier**

link + tabulation : **ecrire le lien plus vite**

div.col + tabulation : **rajoute une colonne dans html**

shift + command + K : **pusher**

shift + control + V : **affiches les commandes récentes afin de les coller**

shift + F6 : **refactor the same word dans tous le doc**

ctrl + alt + L : **mise en forme auto**
