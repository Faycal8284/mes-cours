- [***Table of contents***](# )
    - [Object](#objects)
        - [Objects attribute access](#object-attribute-access)
        - [Objects keys and values](#object-keys-and-values)



# Objects
JavaScript est conçu autour d'un paradigme simple, basé sur les objets. Un objet est un ensemble de propriétés et 
une propriété est une association entre un nom (aussi appelé **keys**) et une **values**.

## object attribute access
Permet d'atteindre les attributs de l'objet souhaité en utilisant une notation avec crochet d'autant plus si 
les attributs ne sont pas connu au départ ou si il sont variable par exemple si nous décidons
de récupérer que le nom et le prénom ou une combinaison de plusieurs autre attributs nous pourrons tout de meme atteinde les attributs f
Permet d'avoir acces à l'attributs d'un objet lorsque l'on ne connait pas le nom de cet attributs à l'avance.
```
const user{
        firstName:'seb'
        lastName:'leProf'
        age: 34,
        billionnaire: false
        eyes: 'blue'
};
const attrs:[lastName', 'age'];

attrs.forEach(attr=> console.log( user.[attr]));
```

## object keys and values

Certaines données sont parfois non pas en formes d'array mais en liste d'object
Afin de pouvoir manipuler plus aisément ces listes via des méthodes il est préférable d'avoir un array.
Il est possible grace à `Object.keys` qui récupére les clés des objets ou `Object.values` qui récupére les valeurs
de les transformer en array.

```
const hostels= {
   hotel1: {
            roomnumbers:2
            id:1
            }
   hotel2:{
            roomnumbers:2
            id:1
            }
   hotel3:{
             roomnumbers:2
             id:1
            }

const keys= Object.values(hostels)
// [hotel1,hotel2,hotel3]
Nous renvoie que les clés dans un array.

const values= Object.values(hostels)
//hostels:  [[hotel1:...,hotel2:...,hotel3...]
Renvoie hostels entier dans un array
```
