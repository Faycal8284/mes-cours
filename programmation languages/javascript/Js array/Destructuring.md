# Destructuring

## Method
Les deux structures de données les plus utilisées en JavaScript sont Object et Array.
Les objets nous permettent de créer une entité unique qui stocke les éléments de données par clé, et les tableaux nous permettent de rassembler les éléments de données dans une collection ordonnée.
Mais lorsque nous les transmettons à une fonction, il peut ne pas avoir besoin d'un objet / tableau dans son ensemble, mais plutôt de pièces individuelles.
L'assignation de déstructuration est une syntaxe spéciale qui nous permet de «décompresser» des tableaux ou des objets en un tas de variables, car c'est parfois plus pratique. La déstructuration fonctionne également très bien avec des fonctions complexes qui ont beaucoup de paramètres, de valeurs par défaut, etc.

### Object destructuring
Vous avez un objet et vous voulez extraire ses données. Avec ES5 vous procédez ainsi :
```
var identite = { nom: 'Durand', prenom: 'Pierre' };
var nom = identite.nom;
var prenom = identite.prenom;
console.log(nom);  // Durand
console.log(prenom);  // Pierre
```
On a un code simple mais il pourrait rapidement s’alourdir avec un objet complexe ou chargé.

Avec ES6 vous pouvez utiliser cette syntaxe :
```
let identite = { nom: 'Durand', prenom: 'Pierre' };
let {nom, prenom} = identite;
console.log(nom);  // Durand
console.log(prenom);  // Pierre
```
La valeur de la propriété nom de l’objet identite est copiée dans la variable nom.
La valeur de la propriété prenom de l’objet identite est copiée dans la variable prenom.

##### Default Value

Le comportement est exactement le même que lorsqu’on utilise l’extraction d’une propriété d’un objet avec la syntaxe classique, la valeur est undefined :
```
let personne = {nom: 'Durand', prenom: 'Pierre'};
let { nom, prenom, age } = personne;
console.log(nom)  // Durand
console.log(prenom)  // Pierre
console.log(age)  // undefined
```
Mais vous pouvez prévoir une valeur par défaut :
```
let personne = {nom: 'Durand', prenom: 'Pierre'};
let { nom, prenom, age = 20 } = personne;
console.log(nom)  // Durand
console.log(prenom)  // Pierre
console.log(age)  // 20
```

### Array destructuring
Pour un tableau ça fonctionne de la même manière mais avec des crochets :
```
let prenoms = [ 'Pierre', 'Jacques', 'Paul' ];
let [ prenomUn, prenomDeux, prenomTrois ] = prenoms;
console.log(prenomUn); // Pierre
console.log(prenomDeux); // Jacques
console.log(prenomTrois); // Paul
```
Avec un tableau on doit choisir des noms pour les variables puisque à la base on a juste des index.

Mais rien n’empêche de déclarer auparavant les variables :
```
let prenoms = [ 'Pierre', 'Jacques'];
let prenomUn = 'Paul';
let prenomDeux = 'Martin';
[ prenomUn, prenomDeux ] = prenoms;
console.log(prenomUn); // Pierre
console.log(prenomDeux); // Jacques
```
On peut récupérer un nombre limité d’éléments si on veut :
```
let prenoms = [ 'Pierre', 'Jacques', 'Paul' ];
let [ prenomUn, prenomDeux ] = prenoms;
console.log(prenomUn); // Pierre
console.log(prenomDeux); // Jacques
```
Pour le cas où on veut sauter des éléments on utilise cette syntaxe :
```
let prenoms = [ 'Pierre', 'Jacques', 'Paul' ];
let [ , , prenomTrois ] = prenoms;
```