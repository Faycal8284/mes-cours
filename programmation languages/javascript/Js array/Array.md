- [Tables of contents](# )
    - [Array](#Array)
    - [lenght and index](#length-and-index)


# Array
Un array est un tableau qui permet de rassembler plusieurs données ou éléments comme des objets on peut les stocker dans un tableau.
```
const tab = [1,2,3,4];
console.log(tab);
```
La console devrait afficher tous les objets à l'intérieur des crochets ici 1 2 3 4 . 
On peut aussi faire  
`
console.table; 
`
La console affichera un tableau avec ses éléments.

## .length and index

La propriété length indique le nombre d'éléments présent dans le tableau.
Pour demander un élément du tableau avec son index on indique l'index entre des crochets :
`
console.log(tab[0]);
`
Pour connaître la longeur du tableau ;
`
console.log(tab.length);
`
Pour obtenir le dernier élément d'un tableau :
`
console.log(tab[tab.length - 1]);
`