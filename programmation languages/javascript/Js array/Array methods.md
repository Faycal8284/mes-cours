- [***Tables of contents***](#)
    - [***Les méthodes array***](#les-mthodes-array)
        - [map and temp](#map-and-temp)
        - [foreach](#foreach)
        - [filter](#filter)
        - [sort](#sort)
            - [ternay](#ternay)
        - [includes](#includes)
        - [push](#push)
        - [concat](#concat)
        - [splice](#slice)
        - [slice](#map-and-temp)
        - [reduce](#reduce)
        - [find](#find)
        - [every](#every)
        - [some](#some)
        







# Les méthodes array

Les méthodes sont des fonctions préetablis qui facilite énormément les language fonctionnelle sur Js.
Elles permettent de manipuler les array trés aisément.


## .map and .temp

Map itère à travers chaque élément, le transforme d’une manière ou d’une autre,
l’ajoute à un nouveau tableau, et retourne le nouveau tableau.
Autrement dit crée réelement un nouvel objet.
Elle fonctionne de la même façon que l'operateur forEach. La différence c'est que Map renvoit une valeur en créant un nouvel objet.

```
const tab2 = tab.map((user, index) => {
    return 12;
});
console.table(tab2);
```
La console devrait renvoyer 12 pour chaque éléments du tableau. La méthode map créer un nouveau tableau (tab2) à partir du premier (tab: la référence).
Si on ajoute un élément au tableau :

```
const tab2 = tab.map((user, index) => {
    user.index = index;
    return user;
});

console.table(tab);
console.table(tab2);
```

On a modifié le tableau en lui ajoutant une propriété index. Alors, en plus de créer un nouveau tableau, le tableau de référence (tab) sera modifié aussi (c'est une spécificité du javascript). Pour remédier à ce problème il faut écrire le code comme ceci :
On a deux tableau similaire.
```
const tab2 = tab.map((user, index) => {
    const temp = {...user};
    temp.index = index;
    return temp;
});
```
`Temp` va créer une copie temporaire de user. 
On créer donc un nouveau tableau en copiant le tableau de base.
Pour créer le nouvel objet on rajoute les trois petits points comme dans le code ci-dessus. Cela va copier le tableau de base et c'est cette copie qui sera modifié et non pas le tableau d'origine.

## .forEach
Permet de parcourir les éléments de l'array afin d'utiliser les fonctions sur ces éléments..

```
const tab = [user, user2, user3, user4, user5];

tab.forEach(user => {
console.log('user: ', user)
});
```

forEach va parcourir les éléments du tableau un par un et afficher ces éléments qu'on a appelé "user".


## .filter
Il s'agit d'une méthode qui va filtrer les éléments d'un tableau et en renvoyer un autre tableau rempissant la condition déterminée par 
la fonction callback.
Pour afficher les personnes qui ont plus de 30 ans :

```
const filteredArray = tab.filter(user => user.age > 30);
console.table(filteredArray);
On peut utiliser plusieurs méthode a la fois :
const filteredArray = tab
    .filter(user => user.age > 30);
    .map(user => {
        user.lastName = user.lastName + 's';
        return user;
    });
console.table(filteredArray);

```
Cela va filtrer les users en affichant ceux qui ont plus de 30 ans, et cela va ajouter un 's' a la fin de leur nom.
On peut également écrire ce code de la façon suivante (c'est ce qu'on appelle la **programmation fonctionnelle**) :
```
const filteredArray= tab
    .filter(isUnder30)
    .map(addSAtEnd)
;

function isUnder30(user) {
    return user.age > 30;
};

function addSAtEnd(user) {
    user.lastName = user.lastName + 's';
    return user;
};

```
La **programmation fonctionnelle** est le fait on fait enchainer des fonctions sur des méthodes.
Il est possible de déclarer une fonction après, mais de l'utiliser avant comme dans le code ci-dessus.

## .sort

Il s'agit d'un opérateur qui compare les éléments et les tri.
Pour trier les gens en fonction de leur age (du plus jeune au plus vieux) :

```
const sortedArray = tab.sort(user1, user2) =>
    return user1.age - user2.age);

console.table(sortedArray);
```
Si on inverse les deux users, cela va inverser le tri, et afficher les plus agé d'abord.
On utilise la soustraction parce que l'age est un `number`.
Pour trier par des `strings`:
```
const sortedArray = tab.sort(user1, user2) => {
    if (user1.firstName < user2.firstName) {
        return -1;
    } else {
        return +1;
    }
});

console.log(sortedArray);
```
### Ternay
Quand on a une structure conditionnelle aussi simple on peut écrire d'une façon plus simple :
```
const sortedArray = tab
.sort(user1, user2) => user1.firstName < user2.firstName ? -1 : +1;   
```
## .includes
Pour afficher les personnes qui ont un a ou A dans leur prénom :
```
const sortedArray = tab
    .filter(user => user.firstName.includes('a') || user.firstName.includes('A');
```

## .push
Elle sert a ajouter un élément ou un array à l'array existant à la fin de celui-ci.
Créer un tableau vide et ajouter des éléments à ce tableau :

```
const tab2 = [];

const baby1 = {
    name: 'basile'
};

const baby2 = {
    name: 'achi'
}

tab2.push(baby1, baby2);

console.table(tab2);
```
## .concat

La méthode concat() est utilisée afin de fusionner un ou plusieurs tableaux.
Cette méthode ne modifie pas les tableaux existants, elle renvoie un nouveau tableau.


```
const tab2 = [1, 2, 3];
const tab3 = [4, 5, 6];

const tab4 = tab2.concat(tab3);

console.table(tab4);
```
On peut aussi procéder comme ceci en créant une copie de tab2 et tab3 :
```
const tab2 = [1, 2, 3];
const tab3 = [4, 5, 6];

const tab4 = [...tab2, ...tab3];

console.table(tab4);
```
## .splice

Elle permet de modifier le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments à meme le tableau. On peut donc vider ou remplacer une partie d'un tableau, mais le tableau de base sera modifié aussi.
Ajouter un élément 6 avec splice :
```
const tab2 = [1, 2, 3, 4, 5, 7];

tab2.splice(start:4, deletCount:0, items:6);

console.log(tab2);
```
Ici le 4 représente l'index (l'endroit ou on veut faire l'ajout) c'est à dire après le 5. Le 0 indique qu'on ne veut rien supprimer, et le 6 l'élément qu'on veut rajouter.

Supprimer un élément a partir du 5 eme :
```
const tab2 = [1, 2, 3, 4, 5, 7];

tab2.splice(start:5, deletCount:1);
console.log(tab2);
```

## .slice
Il créer un nouveau tableau en copiant une partie du tableau de base sans le modifier.
Copier les éléments 2 a 4 et les mettre dans le nouveau tableau :
```
const tab2 = [1, 2, 3, 4, 5, 6];

const tab3 = tab2.slice(1, 4);
console.log(tab3);
console.log(tab2);
```

On copie a partir de l'index 1 jusqu'a l'index 4 (l'index 4 ne sera pas copié).
La console affichera "2,3"

## .reduce

C'est un accumulateur qui permet d'accumuler des éléments en créant un nouvel array on peut ainsi récupérer des éléments de notre array de base.
```
const tabNumber = [2, 5, 10, 2, 33];

const total = tabNumber.reduce(acc, currentValue) => acc = acc + currentValue, 0);
console.log(total);
```
Le résultat est égal a 52.

Exemple avec un objet :
```
const tabNumber = [
{firstName: 'toto', age: 12, hobbies: {'foot', 'playstation'},
{firstName: 'tata', age: 13, hobbies: {'foot', 'playstation'},
{firstName: 'titi', age: 14, hobbies: {'foot', 'playstation'},
{firstName: 'tutu', age: 15, hobbies: {'foot', 'playstation'},
]

const total = tabNumber.reduce((acc, currentValue) => acc.concat(currentValue.hobbies));
```
Un array avec les hobbies sera crée.
On peut aussi écrire :

```
const total = tabNumber.reduce((acc, currentValue) => [...acc, ...currentValue.hobbies]);

console.log(total);

```

## .find

Permet de chercher un élément dans notre array.
L'opérateur `.find` retourne un booléen donc soit `true` soit `false`.

```
const hoteltofind= hostels.find(prédicats hotel=> hotel.name === 'hotel ocean');
console log(hoteltofind)
```

La console affichera true si "hotel ocean" existe dans l'objet hostels et false si il n'existe pas.
Attention `.find ` à plusieurs restricitions : 
* il s'arrete de chercher dés qu'il trouve l'élément souhaiter mais si il y a par exemple 2 'hotel ocean' il ne cherchera pas plus loin que le premier.
* `.find`  ne peut pas être chainer avec d'autre fonctions. Car `.find` ne renvoie pas un tableau mais un objet. 

## .every

Renvoie un booléen.Renvoie `true` si *tout* les éléments de l'array repondent aux prédicats sinon false.
En effet si un seul élément de l'objet ne répond pas au prédicats alors false.
```
const allhotelshavepool=hostels.every=(hotel=> hotel.pool === true);
console.log (allhotelshavepool);
```
Dans ce cas si tout les hotels on un piscine la console renvoiera 'true' mais si un seul hotel n'en a pas alors'false'
On peut par exemple si toutes les personnes d'un panels à bien moins de 30 ans.

## .some

Renvoie un booléen.Opérateur inverse de `.every` car il renvoie 'true' si un seul des élément est trouvé.
Dans notre cas il suffit qu'un seul hotel posséde un piscine pour qu'il renvoie 'true'
```
const allhotelshavepool=hostels.some=(hotel=> hotel.pool === true);
console.log (allhotelshavepool);
```
