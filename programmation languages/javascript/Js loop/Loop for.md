

# For
La boucle for est composée de trois expressions optionnelles séparées
par des points-virgules et encadrées entre des parenthèses qui sont suivies par une instruction à exécuter dans la boucle.
Dans le for seul la condition de continuation doit toujours apparaitre et ne peut pas etre en dehors.
```
for ( initialisateur, condition de continuation(prédicats,booléen),défini ce que l'on doit faire)
for (let i = 0 ; i < 20 ;  i=i+1){

  console.log('toto');
}
//return 20 fois toto
```
### Boucle Infinie

Pour tuer la  machine  d'une personne qu'on aime pas vraiment
```
for (let i = 0 ; true ; i++ ) {
    console.log('toto' + i);
}
// return Infinie
```







`
