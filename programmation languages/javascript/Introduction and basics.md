- [***Table of contents***](#)
    - [***Javascript***](#Javascript) 
        - [***Les élémentaires de Javascript***](#les-lmentaires-de-javascript)
            - [Les variables](#les-variables)
            - [Les valeurs primitives](#les-valeurs-primitives)
            - [Les fonctions](#les-fonctions)
            - [Les objets](#les-objets)
            - [Les préicats](#les-prdicats)
            - [Structure conditionnelle if else](#la-structure-conditionnelle-if-else)
        - [Quelques outils](#quelques-outils)    
            - [Query selector](#queryselector)
            - [Linker html et js](#linker-html-et-js)
    

# Javascript

Javascript est un language de programmation de scripts principalement employé dans les pages web interactives, il dynamise ces dernières.Il est aussi utilisé pour les serveurs avec l'utilisation de node.js.

## Les élémentaires de Javascript

### Les variables

Une variable est un moyen de stocker une valeur.
Les variables sont typéés dynamiquement,ce qui veut dire que l'on n'a pas besoin de spécifier le type
de contenu que la variable va contenir.
On peut déclarer une variable à l'aide de `var`,`let` ou `const`.

`let x='bonjour'`

### Les valeurs primitives

Une valeur primitives est une donnée quin'est pas un objet et n'a pas de méthode.
En Javascript il y a 6 types de données primitives.
* `String` : chaine de caractère ou données textuelles

* `Number` : géré pour répresenter des nombres

* `Booléan` = représente le résultat d'une assertion logique et peut avoir deux valeur (**true**) pour le vrai et (**false)**
pour le faux.

* `Null` : le type **Null** ne posséde qu'une valeur : `null` renvoie en général volontairement vers un objet ou une adresse invalide ou inéxistante.

* `Undefined` : une variable à laquelle on n'a pas affecté de valeur vaudra `undefined`.

* `Symbol` : représente une donnée unique et inchangeable qui peut-être uilisée afin de représenter des identifiants pour des propriétés d'un objet.

### Les fonctions

Une fonction correspond à un bloc de code nommé et réutilisable et dont le but est d’effectuer une tâche précise.
Les fonctions définies dans le langage sont appelées fonctions prédéfinies ou fonctions prêtes à l’emploi car il nous suffit de les appeler pour nous en servir.
Pour être tout à fait précis, les fonctions prédéfinies en JavaScript sont des méthodes. Une méthode est tout simplement le nom donné à une fonction définie au sein d’un objet.


```const number1 = 5;
const number2 = 3;

function addition (param1, param2) {
  return param1 + param2;
}

function soustraction (param1, param2) {
  return param1 - param2;
}

const result = addition(number1, number2);
const result2 = soustraction(number1, number2);

console.log(result);
console.log(result2);
```

### Les objets
Un objet est une entité qui va contenir plusieurs variables
Dans l'expemple suivant `me `est l'objet et contient les variables
**nom, prénom, age**

```
const me = {
nom : 'Bouas',
prenom: 'Faycal',
age : 37,
};
```
### Les prédicats

Les fonctions de prédicats sont des fonctions qui retourne comme résultat vrai ou faux ce sont des fonctions de type boléen.
Dans la fonction suivante la machine retournera vrai si je suis agé de plus ou égale à 18ans et faux pour mois de 18ans.
Nous pouvons combiner plusieurs prédicats entre eux avec `&&` qui signifie (et) et `||` qui signifie (ou) .En effet la fonction retourne que si je m'appelle Faycal et
j'ai plus ou égale à 18 ans alors ce sera `true`.  


```
const isMajor = me.age >= 18;
const isMajorAndIsName = (me.prenom === 'Faycal') && (me.age >= 18);

console.log(isMajorAndIsName);
```

### La structure conditionnelle if else

L'instruction `if / else` exécute un bloc de code si une condition spécifiée est vraie. Si la condition est fausse, un autre bloc de code peut être exécuté.

L'instruction `if / else` fait partie des instructions "conditionnelles" de JavaScript, qui sont utilisées pour effectuer différentes actions en fonction de différentes conditions.

En JavaScript, nous avons les instructions conditionnelles suivantes:

* Utilisez `if` pour spécifier un bloc de code à exécuter, si une condition spécifiée est vraie
* Utilisez `else` pour spécifier un bloc de code à exécuter, si la même condition est fausse
* Utilisez `else if` pour spécifier une nouvelle condition à tester, si la première condition est fausse
* Utilisez le commutateur pour sélectionner l'un des nombreux blocs de code à exécuter

```
const person = {
    firstname : 'Faycal',
    lastname : 'Bouras',
    age : 37,
    gender : 'M',
    adresse : 'avenue claude debussy',
    etudes : 'Mes etudes',
};


if (person.gender === 'F') {
  console.log("coucou je suis une femme");
} else if (person.gender === 'M') {
  console.log("coucou je suis un homme");
} else {
  console.log("inconnu");
}
```
On peut aussi l'intégrer dans une autre fonction.

```
const moi = {
    firstname : 'faycal',
    lastname : 'bouras',
    age : 37,
    gender : 'M',
    adresse : 'avenue claude debussy',
    etudes : 'Mes etudes',
};

function sayGender(person) {
    if (person.gender === 'F') {
      console.log("coucou je suis une femme");
    } else if (person.gender === 'M') {
      console.log("coucou je suis un homme");
    } else {
      console.log("inconnu");
    }
}
sayGender(moi);
La fonction renvoie "coucou je suis un homme"
```

## Quelques outils
### Queryselector

Permet de modifier du **Html** avec du **Js**.
Prenons un titre 'h1' situé dans notre document html.
```
const person = {
    firstname : 'Faycal',
    lastname : 'Bouras',
    age : 37,
    gender : 'M',
    adresse : 'avenue claude debussy',
    etudes : 'Mes etudes',
};

const title = document.querySelector("h1");
title.textContent = person.firstname + " " + person.lastname;
```
Le 'h1' de la page html devrait etre remplacé par 'Faycal Bouras'.

### Linker Html et Js 

Pour linker Html avec Js on utilise la balise suivante:
`<script src="index.js"></script>`


