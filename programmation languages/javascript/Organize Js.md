# Organize Js

La séparation entre est essentiel entre:

* Le code proprement dit regroupant les fonctions et méthodes
* Les données que l'on manipule 

Il faut donc exporter les données vers le fichier de code grace à

`export const données={données 1,données2}` 

à taper dans le fichier des données
et importer dans le fichier de code grace

`import {données} from "fichier données".`

