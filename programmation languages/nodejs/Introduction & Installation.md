# Node Js

NodeJS c’est une plateforme de developpement qui met à disposition plusieurs bibliothèques JavaScript directement sur 
votre machine. Ce n’est pas un serveur, ni un framework, mais un applicatif permettant d’interagir par le biais 
du langage JavaScript et ainsi effectuer des actions en fonction des entrées / sorties détectées.

NodeJS repose entièrement sur le moteur V8 pour fonctionner et permet d’avoir des performances de très haut niveau. 
D’ailleurs une des utilisations possibles de NodeJS consiste à s’en servir en tant que Serveur Web. Il permet de réaliser 
les mêmes actions que d’autres langages comme PHP ou Python et s’impose de plus en plus dans ce domaine.

## Installation

* Installer node dans le dossier serveur et saisir dans la console : `cd server` puis `npm i`,
ou Aller dans package json et cliquer sur `npm install`.
* Saisir `npm install -g concurrently`
* Aller dans package.json, show npm script et cliquer sur watch (a faire une fois, on aura plus qu'a ouvrir le serve la prochaine fois)
* Cliquer sur Serve (ca va lancer le watcheur et le serveur)
