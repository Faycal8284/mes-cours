- [Tables of content](# )
    - [Crud](#crud)
        - [Méthodes et requêtes](#mthodes-de-requte-http)
            - [CRUD ou principales requêtes](#les-principales-requetes-ou-crud)
                - [Get](#get)
                - [Post](#post)
                - [Put](#put )
                - [Patch](#patch)
                - [Delete](#delete)



# Crud
L'acronyme informatique anglais CRUD (pour create, read, update, delete).


## Méthodes de requête HTTP

HTTP définit un ensemble de méthodes de requête qui indiquent l'action que l'on souhaite réaliser sur la ressource
 indiquée. Bien qu'on rencontre également des noms (en anglais), ces méthodes sont souvent appelées verbes HTTP

### Les principales requetes ou CRUD
* Create = Post
* Read = Read
* Update = Put or Patch
* Delete = Delete
#### Get
Donne une répresentation sur le navigateur des données.
Permet donc uniquement de récuperer des données.
```Javascript
app.get('/api/users', (req, res) => {
    return res.send(users);
});
```

#### Post
Créer et envoie l'objet nouvellement créer vers la base de données.
Ici nous voyons que le nouvelle élément crée `newUser` pas par une requéte body`req.body`
en effet c'est sur la plateform api (postman par ex) dans body que l'on créer ce nouvelle objet.
```Javascript
app.post('/api/users',
    (req, res) => {
        const newUser = req.body;
        const id = users.length + 1;
        users.push({...newUser, id});
        return res.send(users);
    });
```

#### Put
Met à jour en remplaçant un élément existant par un nouveau.
```Javascript
app.put('/api/users/:id',
    (req, res) => {
        const newUser = req.body;
        const id = parseInt(req.params.id);
        const index = users
            .findIndex((user) => user.id === id);
        users[index] = {...newUser, id};
        return res.send(users);
    });
```

#### Patch

Met à jour une propriété de l'objet. Permet de faire des changement partiel.
```Javascript
app.patch('/api/users/:id',
    (req, res) => {
        const newUser = req.body;
        const id = parseInt(req.params.id);
        const index = users
            .findIndex((user) => user.id === id);
        users[index] = {...users[index], ...newUser};
        return res.send(users);
    });
```

#### Delete

Permet de supprimer un ou plusieurs éléments.
```Javascript
app.delete('/api/users/:id',
    (req, res) => {
        const id = parseInt(req.params.id);
        users = users.filter((user) => user.id !== id);
        return res.send(users);
    });
```
