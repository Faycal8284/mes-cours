- [***Tables of contents***](#)
    - [Flexbox](#flexbox)
        - [exemple](#exemple)
        - [display](#display)
        - [flex-direction](#flex-direction)
        - [flex-wrap](#flex-wrap)
        - [justify-content](#justify-content)
        - [align-content](#align-content)
        - [align-items](#align-items)

# Flexbox

Les flexbox sont des box qui permettent de disposer  notre page comme l'on souhaite
avec différentes box et disposition possible.

## Exemple
Dans un premier temps il faudra définir une taille height et width à notre box
qui sera à l'intérieur d'un container et par la suite nous pourront lui définir des
attributs.

``` 
<div class= "container"</div>
<div class= "box" </div>

.box{
        height:100px;
        width:100px;
        flex-wrap:reverse-wrap;
        justify-content:space-evenly
}
```

Il existe des dizaines d'atttributs permettant la disposition des box c'est pour cela
que l'on peut se réferer à des sites tels que **css-tricks.com**

## display
Cela définit un conteneur flexible; en ligne ou bloc selon la valeur donnée. Il permet un contexte flexible pour tous ses enfants directs.
```
.container {
display:flex}
```

## flex-direction
Définit la direction dans laquelle les éléments flexibles sont placés dans le conteneur flexible. 
 ```
 .container {
 flex-direction: row
 ```
* **row** : gauche à droite
* **ltr** : droite à gauche
* **row-reverse** : droite à gauche
* **column** : haut vers bas
* **column-reverse** : bas vers haut

## flex-wrap

Par défaut, les éléments flexibles essaieront tous de tenir sur une seule ligne. 
Vous pouvez modifier cela et autoriser les éléments à se superposer en fin de ligne selon les besoins avec cette propriété.

 ```
 .container {
 flex-wrap: nowrap
 ```
* **nowrap** : les éléments seront sur une ligne
* **wrap** : les éléments seront sur plusieurs lignes du haut vers le bas.
* **wrap-reverse** : les éléments seront sur plusieurs lignes du bas vers le haut.

## justify-content
Ceci définit l'alignement le long de l'axe principal. 
Il permet de distribuer un espace libre supplémentaire lorsque tous les éléments flexibles d'une ligne sont rigides ou flexibles mais ont atteint leur taille maximale. 
Il exerce également un certain contrôle sur l'alignement des éléments lorsqu'ils dépassent la ligne.

* **flex-start** : les élément sont justifié au début selon notre direction.
* **flex-end** : les élément sont justifié a la fin selon notre direction.
* **start** : les élément sont justifié au début selon la direction du texte.
* **end** : les élément sont justifié à la fin selon la direction de notre texte.
* **center** : les box seront aligné au centre.
* **space-between** : les box sont disposé tout le long de la ligne avec un élément au début un à la fin et le meme espace entre 
toutes les box.
* **space-around** : les box sont disposé tous le long de la ligne avec le
meme espace entre chaque box aussi bien au début qu'à la fin.
* **space-evenly** : les éléments sont distribués de sorte que l'espacement entre deux éléments (et l'espace sur les bords) soit égal.

## align-content
Cela aligne les lignes d'un conteneur flexible à l'intérieur q
uand il y a de l'espace supplémentaire dans l'axe transversal, 
similaire à la façon dont justification-contenu aligne les éléments individuels dans l'axe principal.
Note: this property has no effect when there is only one line of flex items.
```
.container{
align-content:fles-start
}
```

On retrouvera les memes attributs que pour `justify-content` avec juste l'élément
`stretch` en plus qui étire le contenu.

## align-items

Cela définit le comportement par défaut de la disposition des éléments flexibles le long de l'axe transversal sur la ligne actuelle. 
Considérez-le comme la version à contenu justifié pour l'axe transversal (perpendiculaire à l'axe principal).

On retrouvera les memes attributs que pour `justify-content` avec juste l'élément
`stretch` en plus qui étire le contenu sur l'ace transversal.