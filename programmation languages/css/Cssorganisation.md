# Css Organisation
Il est important de bien organiser ces feuilles de styles 
afin de s'y retrouver et d'autant plus lorsque les projets deviennent important
par la taille.

## How to organise

* Créer un ficher **assets** dans lequel on créera un dossier **css*
qui contiendra toutes les feuilles de styles.
* Créer plusieurs dossier en fonction du nombre de page html
et créer des sous dossiers en fonction de la partie de la page html sur laquelle on travail
* Dans ses sous dossiers il faut créer un fichier principale **stylesheet.css**
par exemple.
* Créer ensuite des feuilles css pour les partie souhaitées 
* Une fois ces dernières créer les importer dans la page principale **stylesheet.css**
qui sera la seule linker avec la page html.
* Pour importer il suffit de faire :

`@import url('feuilles css des sous parties')` 