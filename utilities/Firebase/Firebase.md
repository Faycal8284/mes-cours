# firebase

Firebase est un outil permettant de créer un site afin d'avoir un rendu final de notre
code.

Se connecter sur firebase et créer son compte.
Pour voir ses projets aller sur la console.

### install firebase

Pour installer firebase sur l'ide il faut l'installer via le terminal.

`npm install -g fire-tools`

Par la suite créer une entrée pour l'host en créant un fichier html index dans
la racine du projet via 

`<a href="...">`

Avant de déployer notre site il faut initialiser l'hébergement.

`firebase init hosting`
Choisr le fichier racine ` ./ ` et suivre les étapes. 

Enfin il ne reste plus qu'a deployer.

`firebase deploy`.

Si le déploiement ne se fait pas il faut appuyer séléctionner disable cache
dans l'inspecteur de notre page html.

#### Parametrer firebase sur notre ide

Dans firebase.json commencer par ignorer tout les documents inutiles à la création de votre
page web tels que ` node modules` ou `firebase.json` et les packages inutiles à l'affichage de notre page.

Les fichiers étant ignorer il est préférable de déplacer les fichiers utiliser  tels que `bootstrap.min.css`dans notre page
dans un dossier du type vendors.


  

 

