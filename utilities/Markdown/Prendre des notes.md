# Prise de notes
## Les titres 2
Mettre des # afin de structurer les titres.

Un seul # définira donc le titre principal du document (on met un seul titre principal à un document).

On peut avoir plusieurs titre de meme niveau.

ex: Titre 1 #                  
Titre 2 ##
Titre 3 ##                               
Titre 4 ### Titre 5 ### Titre 6 ### etc..

## Inserer du code

Afin d'inserer du code il faut entourer ce dernier de " ` "
pour un code court et " ``` " pour un code long.

ex: ` ceci est du code court`

Il est possible de determiner quel est le language utilisé en le définissant après 
les " ``` ".

ex: `javascript 
const = 4 `

## Les styles 

Mettre en gras: entourer de ** ** sans espace
Mettre en italique: entourer de _ _ sans espace
Ajouter des citations: commencer la phrase par >  
Pour créer des liens: entourer de "[ ]" + (url) plus des parenthèses avec l'url

ex: **Bonjour**
     _Bonjour_
>Bonjour

[BONJOUR](white-rabbit.com)   

## Les listes

### Listes à puces

Mettre des " * " avant chaque texte.

ex:
* liste1
* liste 2
 
### Liste numéroté

Mettre des nombres devant suivi d'un point

1. liste1 
2. liste2
3. liste3

   
 