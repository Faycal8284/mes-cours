# Repository ou repo
Endroit ou nous allons stocker l'intégralité de notre projet

## Comment envoyer un fichier vers le GIT
Il faut tous d'abord activer git grace à "git init" dans le terminal.
L'onglet git apparait en haut à droite.
Si notre fichier est de couleur **rouge** cela signifie que git ne connait
pas encore ce fichier.
Afin que git le reconnaisse il faut cliquer droit sur le fichier
aller dans git et cliquer sur add.
Il devient **vert** et git le reconnait.

### Commit

Pour l'envoyer sur git il suffit de cliquer sur la flèche verte
et ainsi d'accèder à la page d'envoie ou de commit.

Sur cet page séléctionner le ou les fichier 
Taper un message.
Et le commit ou le pusher afin qu'il soit envoyer vers git 
et stocker sur notre pc.

>Pour commiter nos fichiers il faut d'abord créer un repo sur gitlab ou autres.
>et define remote avec le lien url de notre repo créer sur le site.

## Ouvrir un repo sur l'ide

* Il faut tout d'abord cloner ou copier le lien git sur le site lié à notre repo.
* Cliquer sur l'onglet VCS puis checkout from version control puis git et coller l'url.
* Le projet s'ouvre dans notre ide.

Si nous modifions un fichier ce dernier devient **bleu**.
Puis blanc après l'avoir commiter.