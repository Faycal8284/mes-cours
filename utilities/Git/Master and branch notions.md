# Notions de Master et de branch


Le master est le repo ou va etre stocker le projet vérifié et valider.
Afin de ne pas créer des conflits dans le projet il est important de ne pas 
envoyer ses modifications dans le master mais dans des branches 
Afin que ces dernières soit valider par le chef de projet puis intégrer au master.

On peut passer de la master à la branch en faisant checkout en bas à droite de l'ide.
>Il est impératif de committer les changement avant de checkout.
>

**Envoyer notre branch vers le master**
Sur la partie internet on se met sur la partie principale du projet et cliquer sur merge request 
c'est à dire de regrouper ou compiler notre branche avec le master.
>Merger from branch to master

Du coté du chef de projet il pourra ainsi donc merger la branche et donc intéger le 
nouvau fichier au projet.

**Pull**

Pour les récuperer sur l'ide on va dans la master on clique sur la fleche bleu 'pull'
et ainsi on récupére le fichier valider.
