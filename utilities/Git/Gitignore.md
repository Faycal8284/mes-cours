# Gitignore

Gitignore est un fichier que l'on crée à la racine afin de ne pas commiter
et pusher les éléments inutiles à notre projet sur le git tels que _node modules_
ou _pack json_.

## Création du fichier Git Ignore

* Créer un fichier  **.gitignore** dans la racine
* Dans ce fichier insérer tout les fichiers que l'on ne veut pas envoyer
en commencant par un " **.**".
* Si un fichier que l'on souhaite ignorer est toujours présent on peut le remove
avec la commande suivante (attention le fichier sera supprimé il faudra le réinstaller
aprés le commit)

`git rm -rf ."nom du fichier`