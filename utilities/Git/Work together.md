# Work Together on a project

## Our side

Afin de pouvoir travailler correctement de notre coté il est important de 
suivre certaines étapes

* Puller ou récuperer la master à l'aide de la fléche bleue
* Créer une **branche** avec un nom de features pour apporter les modifications à notre travail
* **Commiter et pusher** sur notre branche
* Cliquer sur **merge request** sur Gitlab
* Choisr le chef de projet ou responsable du projets qui gérera les conflits
* Cliquer sur submit pour envoyer la merge

## Project manager side

* Le chef de projet récupére la notification et vérifie
* Vérification du code en cliquant sur **changes**
* En cas de conflit cliquer sur **resolve conflict**
* Le chef de projet contacte le développeur afin qu'il résolve les conflits

## Our side after manager controls

* Se mettre sur la master et récuperer en clickant sur la flèche bleue
* Retourner sur notre branche et à l'aide de vcs git clicker sur **merge changes**
* Clicker sur master et la merger dans la branche
* Résoudre les conflits en clickant sur le fichier merge un par un
* Deux fenètres apparaissent avec notre version à gauche et le version de l'autre développeur
à droite
* Modifier le code et clique sur apply
* Signaler que la merge à été faite en push
* Cliquer sur merge dans la request pour finir

