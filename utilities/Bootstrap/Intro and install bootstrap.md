- [Tables of contents](#)
     - [Bootstrap](#bootstrap)
         - [Comment installer Bootstrap](#comment-installer-bootstrap)
         - [Npm](#npm)
         - [Activer bootstrap dans html](#activer-bootstrap-dans-html)
         - [Html et Css](#html-et-css)
            - [Grille](#grille)
            - [Ajouter une feuille de style](#ajouter-une-feuille-de-style)
            

# Bootstrap

Bootstrap et un framework permettant d'accéder aux fonctionnalités de css et de structurer notre travail beaucoup plus facilement.
## Comment installer bootstrap
Ouvrir le terminal dans webstorm.
Le terminal permet de donner des commandes comme installer des logiciels.

## Npm

Store passant par **node** permettant d'installer des logiciels ou des outils.
Les commandes: **npm init** puis **npm install bootstrap** ou **npm install nom du logiciels**
Attention!! Bien noté le nom du logiciel ou de l'outils car beaucoup de fake avec virus.

## Activer bootstrap dans html
link+tabulation
Dans href par exemple les deux points ou plus permettent de remonter 
dans les dossiers
**_ouvrir node modules/bootstrap/dist/css/bootstrap.css_**

## Html et css 
Plusieurs autocomplétion provenant de bootstrap apparaissent désormais.
Tels que **_"class="container"_**

* **"Container"** et la class principale qui va contenir tout le contenu du site.
### Grille
Une grille (grid) contient des lignes et des colonnes.
L'unité pricipale est la ligne **(row)** et à l'intérieur de la ligne
on insère les colonnes **(col)**.

ex:

          <div class="row">
              <div class="col numbers">1</div>
              <div class="col numbers">2</div>
              <div class="col numbers">3</div>

### Ajouter une feuille de style 
Pour rajouter un style de préférences dans un autre dossier créer une **"stylesheet"**
Ne pas oublier de mettre le liens dans la page html grace à **"link"**.
Créer le style et l'ajouter à la class ou autres.

ex stylesheet:
```
.numbers {
    text-align: center;
}
```
ex html:

```
        <div class="col numbers">1</div>
        <div class="col numbers">2</div>
```


