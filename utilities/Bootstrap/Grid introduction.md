# GRID ET ORGANISATION DE NOTRE PAGE

Bootstrap permet d'organiser sa page sur une grille composé de 12 column.
## Notion de container
Le container est la class qui va contenir toute notre page.
Il existe deux types de container:
**Container** et **Container fluid**
La diffèrence entre les deux et que le container sera utile 
si nous voulons une page délimité alors que le container fluid va lui prendre 
toute la largeur de la page.
Tous deux se subdivisant toujours en 12 colonnes.
## Row et Column
>Il ne peut pas y avoir de column sans row et inversevement 
pas de row sans column les deux sont interdependants.

12 colonnes possible  on peut donc affecter le nombre 
de colonnes d'unité à notre colonne 
ex: col-4 ainsi notre colonne prendre 4 unités de colonne

>Les colonnes sont prédéfinis pour se coller à gauche de la page

Raccourci rajour de colonnes: div.col+tabulation

**LES OFFSETS**

Permet de ne pas affecter la  ou les colonnes de gauche
ex: 

```
 <div class="col-3 offset-1"></div>
```

dans notre exemple cela aura pour effet de ne pas affecter la colonne de gauche
car nous avons mis offset-1.

**CONTENU VIDE**

Avoir un contenu vide dans une column: **non breaking space** 
Mettre **_&nbsp_** à la place du contenu

